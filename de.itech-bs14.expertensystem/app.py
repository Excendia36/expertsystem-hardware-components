import os

from flask import Flask, render_template, request
import dbmanager.db_connection as db
import objects.objectsDAO as dao
from objects.graphics_card import Graphics_card


app = Flask(__name__)


@app.route("/")
def main():
    """ renders index template """
    return render_template("index.html")


@app.route("/graphics_card")
def graphics_card():
    """ renders gc template """
    return render_template("graphics_card.html")


@app.route("/send", methods=["POST"])
def send():
    """ start pulling data from form input """
    if request.method == "POST":
        gpu = request.form["gpu"]
        ram = request.form["ram"]
        ms = request.form["ms"]
        pu = request.form["pu"]
        price = request.form["price"]

        db.connection()
        criteria = {"gpu_mhz": gpu,
                    "ram_gb": ram, "memorySpeed_mhz": ms, "powerUsage_watt": pu, "price_euro": price}

        results = dao.get_object_from_db("graphic_cards", criteria)
        result = dao.return_filtered_object(Graphics_card, results)

        return render_template("graphics_card.html", result=result)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=int(os.getenv('PORT', 5000)))
