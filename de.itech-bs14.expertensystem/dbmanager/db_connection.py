import psycopg2


def connection():
    # builds db connection
    global con
    con = psycopg2.connect(database="choose_component", user="azubi",
                           password="a1t1AZUBI", host="localhost", port="5432")
    print("Database opened successfully", end='\n\n')


def execute_query(query):
    # prints filter results
    print("\nHere the results:")
    cursor = con.cursor()
    cursor.execute(query)
    row = cursor.fetchone()
    array = []
    while (row != None):
        array.append(__parse_row__(row))
        row = cursor.fetchone()
    return array


def __parse_row__(row):
    if row != None:
        return list(row)
    return None
