from unittest.mock import patch
from unittest import TestCase
# import objectsDAO as dao
# import sys
# sys.path.append(1, 'expertensystem/objects')
# import objects.objectsDAO as dao
from objects.objectsDAO import __init_criteria__
from objects.objectsDAO import __print_filtered_object__
from objects.objectsDAO import get_object_from_db
from objects.cpu import CPU


# from dbmanager import db_connection


class Test(TestCase):
    # def test_filter_component(self):
    #     self.fail()

    @patch('builtins.input', side_effect=[8, 90])
    def test_init_criteria(self, mock_input):
        criteria = {"cores": None, "clockSpeed_mhz": None}
        __init_criteria__(criteria)

        self.assertTrue(criteria["cores"] ==
                        8 and criteria["clockSpeed_mhz"] == 90)

    # def test_get_object_from_db(self):
    #     criteria = {"cores": 12}
    #     array = [1, 'AMD Ryzen 9 5900X', '12', '3700', '3200', '580']
    #     cpu = CPU(array)
    #     get_object_from_db("cpus", criteria)
    #     result = [[1, 'AMD Ryzen 9 5900X', 12, 3700, 3200, 580]]
    #     self.assertEqual(result)

    @patch('builtins.print')
    def test_print_filtered_object(self, mock_print):
        results = [[1, 'AMD Ryzen 9 5900X', 12, 3700, 3200, 580]]
        __print_filtered_object__(CPU, results)
        mock_print.assert_called_with(
            "CPU: AMD Ryzen 9 5900X, 12 cores, 3700 MHz clock speed, 3200 MHz bus speed, 580€")
