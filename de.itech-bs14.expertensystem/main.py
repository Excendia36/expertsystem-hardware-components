from ui import ui
import dbmanager.db_connection as db


def main():
    db.connection()
    ui.start()


if __name__ == "__main__":
    main()
