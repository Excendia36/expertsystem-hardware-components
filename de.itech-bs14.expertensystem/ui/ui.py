from objects.cpu import CPU
from objects.graphics_card import Graphics_card
from objects.monitor import Monitor
from objects.motherboard import Motherboard
from objects.ram import RAM

import objects.objectsDAO as dao


def start():
    while True:
        table = {1: "cpus", 2: "graphic_cards",
                 3: "monitors", 4: "motherboards", 5: "rams"}
        t_parameter = int(input(
            "Which component are you looking for? Type:\n 1 for CPU\n 2 for graphics card\n 3 for monitor\n 4 for motherboard or\n 5 for RAM\n: "))

        print()

        if t_parameter == 1:
            criteria = {"cores": None,
                        "clockSpeed_mhz": None, "busSpeed_mhz": None, "price_euro": None}

            dao.filter_component(criteria, table, t_parameter, CPU)

        elif t_parameter == 2:
            criteria = {"gpu_mhz": None,
                        "ram_gb": None, "memorySpeed_mhz": None, "powerUsage_watt": None, "price_euro": None}

            dao.filter_component(criteria, table, t_parameter, Graphics_card)

        elif t_parameter == 3:
            criteria = {"screenSize_inch": None,
                        "responseTime_ms": None, "resolution_px": None, "price_euro": None}

            dao.filter_component(criteria, table, t_parameter, Monitor)

        elif t_parameter == 4:
            criteria = {"formFactor": None,
                        "socket": None, "memorySupport": None, "price_euro": None}

            dao.filter_component(criteria, table, t_parameter, Motherboard)

        elif t_parameter == 5:
            criteria = {"memoryType_ddr": None,
                        "clockSpeed_ddr": None, "capasity_gb": None, "price_euro": None}

            dao.filter_component(criteria, table, t_parameter, RAM)
