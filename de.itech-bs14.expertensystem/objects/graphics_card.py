class Graphics_card(object):

    def __init__(self, array):
        self.name = array[1]
        self.gpu_mhz = array[2]
        self.ram_gb = array[3]
        self.memorySpeed_mhz = array[4]
        self.powerUsage_watt = array[5]
        self.price_euro = array[6]

    def set_name(self, name):
        self.name = name

    def set_gpu_mhz(self, gpu_mhz):
        self.gpu_mhz = gpu_mhz

    def set_ram_gb(self, ram_gb):
        self.ram_gb = ram_gb

    def set_memorySpeed_mhz(self, memorySpeed_mhz):
        self.memorySpeed_mhz = memorySpeed_mhz

    def set_powerUsage_watt(self, powerUsage_watt):
        self.powerUsage_watt = powerUsage_watt

    def set_price_euro(self, price_euro):
        self.price_euro = price_euro

    def get_name(self):
        return self.name

    def get_ram_gb(self):
        return self.ram_gb

    def get_memorySpeed_mhz(self):
        return self.memorySpeed_mhz

    def get_powerUsage_watt(self):
        return self.powerUsage_watt

    def get_price_euro(self):
        return self.price_euro

    def to_string(self):
        return "Graphics card: " + str(self.name) + ", " + str(self.gpu_mhz) + "MHz GPU, " + str(self.ram_gb) + "GB RAM, " + str(self.memorySpeed_mhz) + "MHz memory speed, " + str(self.powerUsage_watt) + "Watt power usage, " + str(self.price_euro) + "€"
