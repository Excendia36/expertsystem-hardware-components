class Monitor(object):

    def __init__(self, array):
        self.name = array[1]
        self.screenSize_inch = array[2]
        self.responseTime_ms = array[3]
        self.resolution_px = array[4]
        self.price_euro = array[5]

    def set_name(self, name):
        self.name = name

    def set_screenSize_inch(self, screenSize_inch):
        self.screenSize_inch = screenSize_inch

    def set_responseTime_ms(self, responseTime_ms):
        self.responseTime_ms = responseTime_ms

    def set_resolution_px(self, resolution_px):
        self.resolution_px = resolution_px

    def set_price_euro(self, price_euro):
        self.price_euro = price_euro

    def get_name(self):
        return self.name

    def get_screenSize_inch(self):
        return self.screenSize_inch

    def get_responseTime_ms(self):
        return self.responseTime_ms

    def get_resolution_px(self):
        return self.resolution_px

    def get_price_euro(self):
        return self.price_euro

    def to_string(self):
        return "Monitor: " + str(self.name) + ", " + str(self.screenSize_inch) + "inch screen, " + str(self.responseTime_ms) + "ms responce time, " + str(self.resolution_px) + " resolution, " + str(self.price_euro) + "€"
