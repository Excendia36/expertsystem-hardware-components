class Motherboard(object):

    def __init__(self, array):
        self.name = array[1]
        self.formFactor = array[2]
        self.socket = array[3]
        self.memorySupport = array[4]
        self.price_euro = array[5]

    def set_name(self, name):
        self.name = name

    def set_formFactor(self, formFactor):
        self.formFactor = formFactor

    def set_socket(self, socket):
        self.socket = socket

    def set_memorySupport(self, memorySupport):
        self.memorySupport = memorySupport

    def set_price_euro(self, price_euro):
        self.price_euro = price_euro

    def get_name(self):
        return self.name

    def get_formFactor(self):
        return self.formFactor

    def get_memorySupport(self):
        return self.memorySupport

    def get_socket(self):
        return self.socket

    def get_price_euro(self):
        return self.price_euro

    def to_string(self):
        return "Motherboard: " + str(self.name) + ", " + str(self.formFactor) + " form factor, " + str(self.socket) + " socket, " + str(self.memorySupport) + " memory support, " + str(self.price_euro) + "€"
