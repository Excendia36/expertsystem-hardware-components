from dbmanager import db_connection
from ui import ui
from objects.cpu import CPU
from objects.graphics_card import Graphics_card
from objects.monitor import Monitor
from objects.motherboard import Motherboard
from objects.ram import RAM


def filter_component(criteria, table, t_parameter, object):
    """
    processes the filtering of components
    :param criteria: column names in db component's table
    :param table: dict of tables
    :param t_parameter: key of table dict
    :param object: class name
    """
    print("Ok,", table[t_parameter] +
          ". Please enter values, which you prefer.\n")

    __init_criteria__(criteria)

    table_name = table[t_parameter]
    results = get_object_from_db(table_name, criteria)
    __print_filtered_object__(object, results)
    print()


def __init_criteria__(criteria):
    """
    takes criteria dictionary and initializes it
    criteria - column names in db component's table
    :param criteria: dict
    :return: initialized dict
    """
    for key in criteria:
        criteria[key] = input(key.upper() + ": ")


def get_object_from_db(table_name, criteria):
    """
    filters objects and returns fitting object(s)
    :param table_name: component's table name
    :param criteria: column names in db component's table
    :return: array with db object(s)
    """
    query = "SELECT * FROM " + table_name + " WHERE "
    for key, value in criteria.items():
        if value != "":
            query += key + " = '" + str(value) + "' AND "

    return db_connection.execute_query(query[:-5])


def __print_filtered_object__(object, results):
    """
    initializes objects and prints filtered object(s)
    :param object: class name
    :param results: array with db object(s)
    """
    if len(results) > 0:
        for i in results:
            i_object = object(i)

            print(i_object.to_string())
    else:
        print("Sorry, not found")


def return_filtered_object(object, results):
    """
    initializes objects and prints filtered object(s)
    :param object: class name
    :param results: array with db object(s)
    """
    array = []
    if len(results) > 0:
        for i in results:
            array.append(object(i).to_string())
    else:
        print("Sorry, not found")

    return array
