class CPU(object):

    def __init__(self, array):
        self.name = array[1]
        self.cores = array[2]
        self.clockSpeed_mhz = array[3]
        self.busSpeed_mhz = array[4]
        self.price_euro = array[5]

    def set_name(self, name):
        self.name = name

    def set_cores(self, cores):
        self.cores = cores

    def set_clockSpeed_mhz(self, clockSpeed_mhz):
        self.clockSpeed_mhz = clockSpeed_mhz

    def set_busSpeed_mhz(self, busSpeed_mhz):
        self.busSpeed_mhz = busSpeed_mhz

    def set_price_euro(self, price_euro):
        self.price_euro = price_euro

    def get_name(self):
        return self.name

    def get_cores(self):
        return self.cores

    def get_clockSpeed_mhz(self):
        return self.clockSpeed_mhz

    def get_busSpeed_mhz(self):
        return self.busSpeed_mhz

    def get_price_euro(self):
        return self.price_euro

    def to_string(self):
        return "CPU: " + str(self.name) + ", " + str(self.cores) + " cores, " + str(self.clockSpeed_mhz) + " MHz clock speed, " + str(self.busSpeed_mhz) + " MHz bus speed, " + str(self.price_euro) + "€"
