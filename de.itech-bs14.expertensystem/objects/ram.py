class RAM(object):

    def __init__(self, array):
        self.name = array[1]
        self.memoryType_ddr = array[2]
        self.clockSpeed_mhz = array[3]
        self.capasity_gb = array[4]
        self.price_euro = array[5]

    def set_name(self, name):
        self.name = name

    def set_memoryType_ddr(self, memoryType_ddr):
        self.memoryType_ddr = memoryType_ddr

    def set_clockSpeed_mhz(self, clockSpeed_mhz):
        self.clockSpeed_mhz = clockSpeed_mhz

    def set_capasity_gb(self, capasity_gb):
        self.capasity_gb = capasity_gb

    def set_price_euro(self, price_euro):
        self.price_euro = price_euro

    def get_name(self):
        return self.name

    def get_memoryType_ddr(self):
        return self.memoryType_ddr

    def get_clockSpeed_mhz(self):
        return self.clockSpeed_mhz

    def get_capasity_gb(self):
        return self.capasity_gb

    def get_price_euro(self):
        return self.price_euro

    def to_string(self):
        return "RAM: " + str(self.name) + ", DDR" + str(self.memoryType_ddr) + " memory type, " + str(self.clockSpeed_mhz) + "MHz clock speed, " + str(self.capasity_gb) + "GB capasity, " + str(self.price_euro) + "€"
