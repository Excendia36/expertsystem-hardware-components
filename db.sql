### This SQL script creates a database with important values for the project

CREATE TABLE GRAPHIC_CARDS(
                              id serial PRIMARY KEY,
                              Product varchar NOT NULL,
                              GPU_MHz int NOT NULL,
                              RAM_GB int NOT NULL,
                              MemorySpeed_MHz int NOT NULL,
                              PowerUsage_Watt int NOT NULL,
                              Price_Euro int NOT NULL
);

INSERT INTO GRAPHIC_CARDS (Product, GPU_MHz, RAM_GB, MemorySpeed_MHz, PowerUsage_Watt, Price_Euro) VALUES
('Asus RX480-8G', '1266', '8', '7000', '90', '300'),
('ASUS Cerberus-GTX1050TI-O4G', '1455', '4', '7008', '75', '340');

CREATE TABLE CPUS(
                     id serial PRIMARY KEY,
                     Product varchar NOT NULL,
                     Cores int NOT NULL,
                     ClockSpeed_MHz int NOT NULL,
                     BusSpeed_MHz int NOT NULL,
                     Price_Euro int NOT NULL
);

INSERT INTO CPUS (Product, Cores, ClockSpeed_MHz, BusSpeed_MHz, Price_Euro) VALUES
('AMD Ryzen 9 5900X', '12', '3700', '3200', '580'),
('AMD Ryzen 7 3700X', '8', '3600', '3200', '269');

CREATE TABLE MONITORS(
                     id serial PRIMARY KEY,
                     Product varchar NOT NULL,
                     ScreenSize_inch int NOT NULL,
                     ResponseTime_ms int NOT NULL,
                     Resolution_px varchar NOT NULL,
                     Price_Euro int NOT NULL
);

INSERT INTO MONITORS (Product, ScreenSize_inch, ResponseTime_ms, Resolution_px, Price_Euro) VALUES
('LG UltraGear', '24', '1', '1920 x 1080', '150'),
('Samsung S22F350FHU', '22', '5', '1920 x 1080', '107');

CREATE TABLE MOTHERBOARDS(
                     id serial PRIMARY KEY,
                     Product varchar NOT NULL,
                     FormFactor varchar NOT NULL,
                     Socket varchar NOT NULL,
                     MemorySupport varchar NOT NULL,
                     Price_Euro int NOT NULL
);

INSERT INTO MOTHERBOARDS (Product, FormFactor, Socket, MemorySupport, Price_Euro) VALUES
('Asus Prime B450M-K II', 'MicroATX', 'AM4', '2xDDR4', '70'),
('Asus Prime H410M-K', 'MicroATX', 'LGA 1200', '2xDDR4', '80');

CREATE TABLE RAMS(
                     id serial PRIMARY KEY,
                     Product varchar NOT NULL,
                     MemoryType_DDR int NOT NULL,
                     ClockSpeed_MHz int NOT NULL,
                     Capacity_GB int NOT NULL,
                     Price_Euro int NOT NULL
);

INSERT INTO RAMS (Product, MemoryType_DDR, ClockSpeed_MHz, Capacity_GB, Price_Euro) VALUES
('HyperX Fury', '4', '3200', '16', '80'),
('HyperX Fury', '4', '3200', '32', '140');